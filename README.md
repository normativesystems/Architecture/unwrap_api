# Unwrap_API

To further enhance the capabilities of the Norm Engineering system, the Unwrap API was developed this year. It serves a crucial function in the information architecture, facilitating the transformation of RDF (Resource Description Framework) data into JSON format. The RDF data, structured according to the FLINT framework's expressivity, is converted to a JSON format suitable for use within the Norm Editor. This transformation is essential for maintaining the fluidity and usability of data across different modules of the system.

# Folder Internal Organization

The internal organization of the Unwrap Api project folder can be outlined as follows:


app.py: This is the central Python script serving two primary functions:

1. Conversion of RDF data to Json: The functions convert RDF data, which is highly structured and adheres to the FLINT ontology, into a more flexible and widely-used JSON format. This conversion is vital for ensuring that the data, originally stored and managed in the Triplestore system, becomes accessible and editable within the Norm Editor. 

2. Endpoints Definition: Several endpoints are defined. They are: Index Endpoint, Get Graph Names Endpoint,Download Graph Endpoint, Process and Download Graph Endpoint. They can be described as follows. _Index Endpoint_ renders and retirns the contentes of _index.html_. _Get Graph Names Endpoint_ fetches the names of graphs from the connected dataset of Triply and returns the graph names. _Download Graph Endpoint_ downloads the graph data corresponding to the selected graph (iri). It handles gzip compression and returns the data as a downloadable Turtle File. _Process and Download Graph Endpoint_ downloads the specified graph data, converts the processed data to Json using the above mentioned functions, and returns it as downloadable file.  

index.html: is a HTML document designed for selecting graphs from Triply and download them.  

Test Folder: This folder contains various test files. These files are specifically used for testing the functions that convert and  data from RDF to Json. Additionally, within this folder, there is a Jupyter Notebook where the conversion functions have been designed and developed. The notebook can be used to easily test and refine the functions.
