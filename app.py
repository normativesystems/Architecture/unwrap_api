#    Copyright 2023 Nederlandse Organisatie voor Toegepast Natuur-
#     wetenschappelijk Onderzoek TNO / TNO, Netherlands Organisation for 
#     applied scientific research

#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at

#        http://www.apache.org/licenses/LICENSE-2.0

#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

import gzip
import json
from pyld import jsonld
import re
from io import BytesIO

import rdflib
from rdflib.collection import Collection
import urllib.parse
import requests
import dateutil.parser

from flask import Flask, render_template_string, jsonify, request, Response, send_file
from rdflib import Graph, Dataset, Namespace, RDF, URIRef, Literal, XSD, RDFS, OWL, PROV, BNode

app = Flask(__name__)

EDITOR = Namespace("http://ontology.tno.nl/normengineering/editor#")
FLINT = Namespace("http://ontology.tno.nl/normengineering/flint#")
SRC = Namespace("http://ontology.tno.nl/normengineering/source#")
CALC = Namespace("http://ontology.tno.nl/normengineering/calculemus#")
DC1 = Namespace("http://purl.org/dc/elements/1.1/")
DC = Namespace("http://purl.org/dc/terms/")
OA = Namespace("http://www.w3.org/ns/oa#")


def describe_fact_structure(graph,fact):
    if get_frame_id(fact): # Fact has an ID, don't describe its subfacts
        return {
            "frame": get_frame_id(fact),
            "children": [],
            "operatorToJoinChildren": None
        }
    else: # Fact is a blank node
        subfacts = Collection(graph, graph.value(fact, FLINT.hasOperands))
        function = get_function_shorthand(graph,graph.value(fact, FLINT.hasFunction))

        if not len(subfacts): # The blank node has no subfacts, so we return a dummy subfact that the editor discards. That should not be possible, but it happens because of a bug in the export API
            return {
                "children": [],
                "operatorToJoinChildren": None
            }
        else:
            return {
                "children": get_children(graph,fact),
                "operatorToJoinChildren": function
            }


def get_children(graph,fact):
    children = []
    operands = graph.value(fact, FLINT.hasOperands)
    subfacts = Collection(graph, operands)
    for subfact in subfacts:
        children.append(describe_fact_structure(graph,subfact))
    return children

def get_frame_id(iri):
    parts = str(iri).split(str(EDITOR))
    if len(parts) == 2:
        return parts[1]
    else:
        return None
    
def get_function_shorthand(graph, iri):
    parts = str(iri).split(str(FLINT))
    if len(parts) == 2:
        return parts[1]
    else:
        return iri

def is_claimduty_relation(graph,iri):
    return (iri, RDF.type, FLINT.Duty) in graph and (graph.value(iri, FLINT.hasClaimant) or graph.value(iri, FLINT.hasHolder) or graph.value(iri, OWL.sameAs))

def build_item(subject, graph):
    item = {}
    classes = list(graph.objects(subject, RDF.type))

    item['id'] = get_frame_id(subject)
    if FLINT.Act in classes:
        item['typeId'] = "act"
        item['act'] = str(graph.value(subject, RDFS.comment) or graph.value(subject, DC.alternative) or '')
        
        item['creates'] = [get_frame_id(pc) for pc in graph.objects(subject, FLINT.creates)]  # list for creates data
        item['terminates'] = [get_frame_id(pc) for pc in graph.objects(subject, FLINT.terminates)]  # list for terminates data

        # connect act with the identifiers of the action, object, actor and recipient.
        action_id = graph.value(subject, FLINT.hasAction)
        item['actionId'] = str(action_id).split(str(EDITOR))[-1] if action_id else None
        actor_id = graph.value(subject, FLINT.hasActor)
        item['actorId'] = str(actor_id).split(str(EDITOR))[-1] if actor_id else None
        object_id = graph.value(subject, FLINT.hasObject)
        item['objectId'] = str(object_id).split(str(EDITOR))[-1] if object_id else None
        recipient_id = graph.value(subject, FLINT.hasRecipient)
        item['recipientId'] = str(recipient_id).split(str(EDITOR))[-1] if recipient_id else None

        precondition_uri = graph.value(subject, FLINT.hasPrecondition)
        if precondition_uri:
            item['precondition'] = describe_fact_structure(graph,precondition_uri)
        else:
            item['precondition'] = {
                'children': [],
                'operatorToJoinChildren': None
            }

    elif any(item in classes for item in [FLINT.Agent, FLINT.Action, FLINT.Object, FLINT.Fact, FLINT.ComplexFact, FLINT.Duty]) and not is_claimduty_relation(graph,subject):
        item['typeId'] = "fact"
        if FLINT.Agent in classes:
            item['subTypeId'] = "agent"
        elif FLINT.Action in classes:
            item['subTypeId'] = "action"
        elif FLINT.Object in classes:
            item['subTypeId'] = "object"
        elif FLINT.Duty in classes:
            item['subTypeId'] = "duty"
        else:
            item['subTypeId'] = None

        item['fact'] = str(graph.value(subject, RDFS.comment) or graph.value(subject, DC.alternative) or '')


        operands = graph.value(subject, FLINT.hasOperands)
        if operands:
            subfacts = Collection(graph, operands)
            operands_amount = len(subfacts)

        else:
            operands_amount = 0

        item['isComplex'] = operands_amount > 0

        if operands_amount:
            function = get_function_shorthand(graph,graph.value(subject, FLINT.hasFunction))
            
            item['subdivision'] = {
                'children': get_children(graph,subject),
                'operatorToJoinChildren': function
            }


    elif is_claimduty_relation(graph,subject):
        item['typeId'] = "claim_duty"
        item['claimduty'] = str(graph.value(subject, RDFS.comment) or graph.value(subject, DC.alternative) or '')
        # Extract the claimant and holder IDs if available
        claimant_id = graph.value(subject, FLINT.hasClaimant)
        holder_id = graph.value(subject, FLINT.hasHolder)
        duty_id = graph.value(subject, OWL.sameAs)
        if claimant_id:
            item['claimantId'] = get_frame_id(claimant_id)
        if holder_id:
            item['holderId'] = get_frame_id(holder_id)
        if duty_id:
            item['dutyId'] = get_frame_id(duty_id)

    else:
        return None

    item['label'] = str(graph.value(subject, RDFS.label) or graph.value(subject, DC1.description) or '')
    item['comments'] = []

    # Get comments for this frame
    for comment in graph.subjects(RDF.type, OA.Annotation):
        if graph.value(comment,OA.hasTarget) == subject:
            if graph.value(None,PROV.used,comment): continue # Skip objects of prov:used, they have been replaced by a newer one

            comment_output = {
                "content": graph.value(comment,OA.bodyValue),
                "author": graph.value(comment,PROV.wasAttributedTo)
            }

            activity_time = graph.value(graph.value(comment,PROV.wasGeneratedBy),PROV.endedAtTime)
            previous_activity_time = graph.value(graph.value(graph.value(graph.value(comment,PROV.wasGeneratedBy),PROV.used),PROV.wasGeneratedBy),PROV.endedAtTime)

            if activity_time and previous_activity_time: # If there are two activities, the comment has been edited
                comment_output['createdAt'] = dateutil.parser.isoparse(previous_activity_time).strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z'
                comment_output['lastEditedAt'] = dateutil.parser.isoparse(activity_time).strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z'
            else:
                comment_output['createdAt'] = dateutil.parser.isoparse(activity_time).strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z' if activity_time else None
                comment_output['lastEditedAt'] = None

            item['comments'].append(comment_output)
    item['comments'].sort(key=lambda x: dateutil.parser.isoparse(x['createdAt'])) # Sort comments by creation time



    annotations = []
    textfragments = graph.objects(subject, FLINT.hasTextFragment)
    for textfragment in textfragments:
        if textfragment:
            character_range = graph.value(textfragment, SRC.hasCharacterRange)
            if character_range:
                startsAtIndex = graph.value(character_range, SRC.startsAtIndex)
                endsAtIndex = graph.value(character_range, SRC.endsAtIndex)
                hasContent = graph.value(textfragment, SRC.hasContent)
                isFragmentOf = graph.value(textfragment, SRC.isFragmentOf)

                snippet = {
                    'characterRange': [int(startsAtIndex), int(endsAtIndex)],
                    'text': hasContent
                }

                match = re.match(r"^(.*choppr.app/.*)/(Document.*/([a-z0-9A-Z]{4})_?[^/]*)$", isFragmentOf)

                if match:
                    snippet['documentId'] = match.group(1)
                    snippet['sentenceIri'] = match.group(2)
                    snippet['sentenceId'] = match.group(3)

                annotation = {
                    'snippets': [snippet]
                }
                annotations.append(annotation)

    if annotations:
        item['annotations'] = annotations

    return item

def reintroduce_bnodes(graph):
    # This function turns blank nodes that were named by Triply back into blank nodes

    # Collect all nodes that were named by Triply
    named_nodes = []
    for s, p, o in graph:
        if '.well-known/genid' in s and s not in named_nodes:
            named_nodes.append(s)
        if '.well-known/genid' in o and o not in named_nodes:
            named_nodes.append(o)

    # Convert all nodes to blank nodes
    for named_node in named_nodes:
        convert_to_bnode(graph,named_node)

def convert_to_bnode(graph, named_node):
    blank_node = BNode()
    for s, p, o in list(graph):
        if s == named_node:
            graph.remove((s, p, o))
            graph.add((blank_node, p, o))
        if o == named_node:
            graph.remove((s, p, o))
            graph.add((s, p, blank_node))

def transform_json_structure(original_json):
    transformed_json = []

    # print("Frame IDs:", frame_ids) to debug

    for item in original_json:
        # Common fields for all types
        transformed_item = {
            # 'id': item.get('id', ''),
            # 'label': item.get('label', ''),
            # 'typeId': item.get('typeId', ''),
            # 'comments': item.get('comments', []),
            # 'annotations': item.get('annotations', []),
            # Determine 'isComplex' based on frame_ids
            # 'isComplex': item['id'] in frame_ids
        }

        # Specific fields for 'fact' and 'claim_duty' types
        if item['typeId'] == 'fact':
            transformed_item.update({
                'id': item.get('id', ''),
                'label': item.get('label', ''),
                'fact': item.get('fact', ''),
                'typeId': item.get('typeId', ''),
                'subTypeId': item.get('subTypeId', None),
                'annotations': item.get('annotations', []),
                'comments': item.get('comments', []),
                'isComplex': item.get('isComplex', True),  # set on Default on True
                'subdivision': item.get('subdivision', {
                    'children': [],
                    'operatorToJoinChildren': None
                })
            })
        elif item['typeId'] == 'claim_duty':
            transformed_item.update({
                'id': item.get('id', ''),
                'typeId': item.get('typeId', ''),
                'label': item.get('label', ''),
                'claimduty': item.get('claimduty', ''),
                'dutyId': item.get('dutyId', ''),
                'claimantId': item.get('claimantId', ''),
                'actorId': item.get('claimantId', ''), # Wrong property name, included for backwards compatibility with older versions of the editor
                'holderId': item.get('holderId', ''),
                'comments': item.get('comments', []),
                'annotations': item.get('annotations', []),
            })

        # Specific fields for 'act' type
        elif item['typeId'] == 'act':
            transformed_item.update({
                'id': item.get('id', ''),
                'typeId': item.get('typeId', ''),
                'label': item.get('label', ''),
                'act': item.get('act', ''),
                'actionId': item.get('actionId', None),
                'actorId': item.get('actorId', None),
                'objectId': item.get('objectId', None),
                'recipientId': item.get('recipientId', None),
                'precondition': item.get('precondition', {
                    'children': [],
                    'operatorToJoinChildren': None
                }),
                'creates': item.get('creates', []),
                'terminates': item.get('terminates', []),
                'comments': item.get('comments', []),
                'annotations': item.get('annotations', [])
            })

        transformed_json.append(transformed_item)

    return transformed_json

def reformat_children(element):
    for child in element.get('children',[]):
        # Add id property to child
        if 'IRI' in child:
            match = re.match(r"^.*/([a-z0-9A-Z]{4})_?[^/]*$", child['IRI'])
            if match:
                child['id'] = match.group(1)

        # Don't embed separators or objects of containsAsHeader, use the IRI instead
        if 'separator' in child and 'IRI' in child['separator']:
            child['separator'] = child['separator']['IRI']
        if 'containsAsHeader' in child and 'IRI' in child['containsAsHeader']:
            child['containsAsHeader'] = child['containsAsHeader']['IRI']

        child = reformat_children(child)
    return element

def reformat_source(source,base):
    # Apply some custom reformatting to the source to resemble Choppr output
    source['@id'] = base.rstrip('/')
    for item in source['@graph']:
        if item.get('@type') == 'src:Source' or 'src:Source' in item.get('@type'):
            item['@type'] = 'src:Source'
            item = reformat_children(item)
            item.pop('http://ontology.tno.nl/normengineering/editor#hasSelectedId','None') # Don't include selected sentences in source
        if 'IRI' in item:
            item['@id'] = urllib.parse.urljoin(base,item['IRI'])
            item.pop('IRI','None') # Don't include IRI property, otherwise it results in colliding keywords error
    
    return source


def data_to_graph(data):
    # Turn blank nodes that were named by Triply back into blank nodes (this is much faster than going through all individual triples)
    data = re.sub(r"<[^<]+\.well-known/genid/([a-z0-9]+)>", r"_:\1", data)

    # This parses the data and puts it in a single graph, whether it's turtle or trig
    try:
        g = Graph()
        g.parse(data=data, format="ttl")
    except:
        d = Dataset()
        d.parse(data=data, format="trig")
        g = Graph()

        for context in d.contexts():
            g += context
    return g


def process_rdf_data(file_path, graph=None):
    if not graph:
        with open(file_path, "r", encoding="utf8") as f:
            g = data_to_graph(f.read())
    else:
        g = graph

    reintroduce_bnodes(g) # Turn blank nodes that were named by Triply back into blank nodes

    # Initialize entity_dict
    entity_dict = {}

    # Build the entity dictionary
    for subject in g.subjects(RDF.type, None):
        if get_frame_id(subject): # Ignore blank nodes
            item = build_item(subject, g)
            if item is not None:
                entity_dict[str(subject)] = item

    # Convert the entity dictionary to JSON
    json_data = list(entity_dict.values())

    
    # Convert the graph to json-ld
    jsonld_data = g.serialize(format='json-ld')
    sourceDocs = []
    for source in g.subjects(RDF.type, SRC.Source):
        # Determine the base IRI from the IRI of the DecompositionEvent, if available
        base = str(source).rstrip('/') + '/'
        event = g.value(source, SRC.generatedBy, None)
        if event:
            match = re.match(r"^(.*)DecompositionEvent/?$", str(event))
            if match:
                base = match.group(1)

        # Define context and frame for source data
        context = {
            "@context": {
                "choppr": "http://ontology.tno.nl/normengineering/choppr#",
                "src": "http://ontology.tno.nl/normengineering/source#",
                "xsd": "http://www.w3.org/2001/XMLSchema#",
                "po": "http://www.essepuntato.it/2008/12/pattern",
                "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
                "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
                "choppr.recipe": base + "recipe/",
                "choppr.separators": base + "separators/",
                "@base": base,
                "IRI": "@id",
                "typelabel": "src:hasTypeLabel",
                "actorLabel": "rdfs:label",
                "numbering": "src:hasNumbering",
                "content": "src:hasContent",
                "hasOrigin": "src:hasOrigin",
                "containsAsHeader": {
                    "@type": "@id",
                    "@id": "src:containsAsHeader"
                },
                "separator": {
                    "@type": "@id",
                    "@id": "choppr:chunkSeparator"
                },
                "children": {
                    "@id": "src:hasChildElementOrdering",
                    "@container": "@list"
                },
                "decompositionPreprocessors": "choppr:decompositionPreprocessors",
                "decompositionSeparators": "choppr:decompositionSeparators",
                "decompositionRecipe": "choppr:decompositionRecipe",
                "begins": "src:begins",
                "ends": "src:ends",
                "authoredBy": "src:authoredBy",
                "editedBy": "src:editedBy",
                "title": "src:hasTitle",
                "version": "src:hasVersion",
                "decompositionVersion": "src:hasDecompositionVersion",
                "applicableAt": "src:applicableAt",
                "applicableFrom": {
                    "@id": "src:applicableFrom",
                    "@type": "xsd:dateTime"
                },
                "applicableUntil": {
                    "@id": "src:applicableUntil",
                    "@type": "xsd:dateTime"
                },
                "generatedBy": {
                    "@type": "@id",
                    "@id": "src:generatedBy"
                },
                "generates": {
                    "@type": "@id",
                    "@id": "src:generates"
                }
            }
        }
        frame = {
            "@context": context,
            "@embed": "@always",
            "@id": [str(source),str(event)],
            "generates": {
                "@embed": "@never"
            },
            "children": {
                "@embed": "@always",
            }
        }

        # Apply framing to limit data to the current src:Source and apply embedding
        source_data = jsonld.frame(json.loads(jsonld_data), frame) # This takes a lot of time! Perhaps this could be improved by using just the named graph of the source here, if it is available

        sourceDocs.append({
            "jsonLd": reformat_source(source_data,base), # Apply custom reformatting to the source
            "collapsedSentencesIds": [],
            "selectedSentencesIds": [str(id) for id in list(g.objects(source,EDITOR.hasSelectedId))]
        })


    task_data = {
        'type': 'Task',
        'frames': transform_json_structure(json_data),
        'sourceDocs': sourceDocs
    }

    task = g.value(None,RDF.type, CALC.Task)
    if task:
        task_data['id'] = str(task)
        task_label = g.value(task,RDFS.label)
        task_desc = g.value(task,RDFS.comment)
        if task_label:
            task_data['label'] = str(task_label)
            task_data['description'] = str(task_desc)
        interpretation = g.value(None,RDF.type,CALC.Interpretation)
        if interpretation and not isinstance(interpretation, BNode): # Include interpretation iri if it's not a blank node
            task_data['interpretation'] = str(interpretation)
        editor = g.value(task,CALC.hasEditor)
        if editor:
            editor_label = g.value(editor,RDFS.label)
            if editor_label:
                task_data['hasEditor'] = str(editor_label)

    if not task_data['frames'] and not 'id' in task_data and len(task_data['sourceDocs']) == 1:
        # If there is exactly one source and no task data or interpretation data, just return the source
        return task_data['sourceDocs'][0]['jsonLd']
    else:
        return task_data
    
def get_api_token():
    with open('my_token.txt', 'r') as file:
        return file.read().strip()

@app.route('/')
def index():
    return render_template_string(open('index.html').read())

@app.route('/get_graph_names')
def get_graph_names():
    api_token = get_api_token()
    url = 'https://api.normativesystems.triply.cc/datasets/giuliabiagioni/Editor/graphs'
    headers = {
        'Authorization': f'Bearer {api_token}',
        'Content-Type': 'application/json'
    }

    try:
        response = requests.get(url, headers=headers)
        response.raise_for_status()
        graphs = response.json()
        return jsonify(graphs)  # Return the entire JSON response
    except requests.HTTPError as http_err:
        return jsonify({'error': str(http_err)}), 400
    except Exception as err:
        return jsonify({'error': str(err)}), 500

@app.route('/download_graph/<path:graph_iri>')
def download_graph(graph_iri):
    api_token = get_api_token()
    url = f'https://api.normativesystems.triply.cc/datasets/giuliabiagioni/Editor/download?graph={graph_iri}'
    headers = {
        'Authorization': f'Bearer {api_token}',
        'Accept': 'text/turtle'
    }

    try:
        response = requests.get(url, headers=headers, stream=True)
        response.raise_for_status()

        # Check for content encoding and handle accordingly
        if 'gzip' in response.headers.get('Content-Encoding', ''):
            compressed_content = BytesIO(response.content)
            decompressed_content = gzip.decompress(compressed_content.read())
            return send_file(
                BytesIO(decompressed_content),
                mimetype='text/turtle',
                as_attachment=True,
                download_name='graph.ttl'
            )
        else:
            return send_file(
                BytesIO(response.content),
                mimetype='text/turtle',
                as_attachment=True,
                download_name='graph.ttl'
            )
    except requests.HTTPError as http_err:
        return jsonify({'error': str(http_err)}), 400
    except Exception as err:
        return jsonify({'error': str(err)}), 500

@app.route('/process_graph', methods=['POST'])
def process_graph():
    try:
        # Check if the Content-Type header is 'text/turtle'
        if request.headers.get('Content-Type') != 'text/turtle' and request.headers.get('Content-Type') != 'text/trig':
            return jsonify(error='Content-Type must be text/turtle or text/trig'), 400
        # Decond and parse turtle data
        turtle_data = request.data.decode('utf-8')
        graph = data_to_graph(turtle_data)
        # Convert the graph to editor JSON data model
        json_data = process_rdf_data(file_path=None, graph=graph)

        # Return the JSON-LD data as a JSON object
        return jsonify(json_data)

    except Exception as e:
        return jsonify(error=str(e)), 500

if __name__ == '__main__':
    app.run(debug=True)


@app.route('/process_and_download_graph/<path:graph_iri>')
def process_and_download_graph(graph_iri):
    api_token = get_api_token()
    url = f'https://api.normativesystems.triply.cc/datasets/giuliabiagioni/Editor/download?graph={graph_iri}'
    headers = {
        'Authorization': f'Bearer {api_token}',
        'Accept': 'text/turtle'
    }

    try:
        response = requests.get(url, headers=headers, stream=True)
        response.raise_for_status()

        # Handle gzip compression
        if 'gzip' in response.headers.get('Content-Encoding', ''):
            compressed_content = BytesIO(response.content)
            content = gzip.decompress(compressed_content.read())
        else:
            content = response.content

        # Process RDF data
        rdf_data = BytesIO(content)
        processed_data = process_rdf_data(rdf_data)

        # Convert processed data to JSON and return as downloadable file
        return send_file(
            BytesIO(json.dumps(processed_data).encode()),
            mimetype='application/json',
            as_attachment=True,
            download_name='processed_graph.json'
        )
    except requests.HTTPError as http_err:
        return jsonify({'error': str(http_err)}), 400
    except Exception as err:
        return jsonify({'error': str(err)}), 500


if __name__ == '__main__':
    app.run(debug=True)