 FROM python:3.11-alpine
 
 WORKDIR /app
 COPY ./requirements.txt ./
 RUN pip install -r requirements.txt --no-cache-dir
 
 COPY ./app.py ./index.html ./
 
 CMD ["flask", "run", "--host=0.0.0.0"]
